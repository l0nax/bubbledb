package db

import (
	"bytes"
	"sync/atomic"

	"github.com/ironpark/skiplist"
	"go.l0nax.org/typact"

	"gitlab.com/l0nax/bubbledb/internal/dbiter"
	"gitlab.com/l0nax/bubbledb/internal/rel"
	"gitlab.com/l0nax/bubbledb/iterop"
)

// Memtable is basic mem-table implementation based on [skip list]s.
// A memtable is a in-memory data structure which will eventually be flushed to disk.
//
// [skip list]: https://en.wikipedia.org/wiki/Skip_list
type Memtable struct {
	// id is the memtable ID.
	id int

	// list holds the data.
	list skiplist.SkipList[[]byte, []byte]

	// approxSize holds the approximate size of the memtable.
	// The actual memtable size might be lower OR larger than this value.
	approxSize atomic.Int64
}

// NewMemtable returns a new initialized [Memtable] with the given ID.
func NewMemtable(id int) *Memtable {
	return &Memtable{
		id:   id,
		list: skiplist.New[[]byte, []byte](skiplist.BytesComparator[[]byte], skiplist.WithMutex()),
	}
}

// Get returns the value for the given key.
func (m *Memtable) Get(key []byte) typact.Option[[]byte] {
	val, ok := m.list.GetValue(key)
	if !ok {
		return typact.None[[]byte]()
	}

	return typact.Some(val)
}

// Put puts the key-value pair into m.
func (m *Memtable) Put(key, value []byte) {
	size := len(key) + len(value)
	m.approxSize.Add(int64(size))

	_ = m.list.Set(key, value)
}

// Scan returns a [MemtableIterator] for the given (lower, upper) keys.
// If lower is provided, the iterator will start from the beginning of the memtable.
// If upper is provided, the iterator will iterate through all the keys in the memtable
// starting from lower.
func (m *Memtable) Scan(lower, upper iterop.Bound[[]byte]) *MemtableIterator {
	var item *skiplist.Element[[]byte, []byte]
	switch lower.BoundType() {
	case iterop.BoundIncluded:
		item = m.list.Find(lower.Key().Unwrap())
	case iterop.BoundExcluded:
		item = m.list.Find(lower.Key().Unwrap())
		item = item.Next()
	case iterop.BoundUnbounded:
		item = m.list.Front()
	}

	return &MemtableIterator{
		item:  item,
		upper: upper,
	}
}

// ID returns the memtable ID.
func (m *Memtable) ID() int {
	return m.id
}

// ApproxSize returns the approximate size of the memtable.
func (m *Memtable) ApproxSize() int64 {
	return m.approxSize.Load()
}

// MemtableIterator is an iterator over a [Memtable].
type MemtableIterator struct {
	// item is the current element.
	item *skiplist.Element[[]byte, []byte]
	// upper is the upper bound key (the end key).
	upper iterop.Bound[[]byte]
	// stopNext indicates that the next call to Next
	// should not provide the next item.
	// It is used in case of [iterop.BoundIncluded].
	stopNext bool
}

// Next moves over to the next element in the iterator.
// If the next element could be retrieved, true is returned.
// Otherwise false is returned.
func (m *MemtableIterator) Next() bool {
	if m.item == nil || m.stopNext {
		m.item = nil
		return false
	}

	m.item = m.item.Next()
	if m.item == nil {
		return false
	}

	// stop if a upper bound has been provided and found
	itemKey := m.item.Key()
	switch m.upper.BoundType() {
	case iterop.BoundIncluded:
		if bytes.Equal(m.upper.Key().Unwrap(), itemKey) {
			// prevent that a call to Next goes through
			m.stopNext = true
		}
	case iterop.BoundExcluded:
		if bytes.Equal(m.upper.Key().Unwrap(), itemKey) {
			m.item = nil

			return false
		}
	case iterop.BoundUnbounded:
		// ignore it, already handled in the if above
	}

	return true
}

// IsValid returns true if the iterator is valid.
// If false is returned, methods like Value or Key will panic.
func (m *MemtableIterator) IsValid() bool {
	return m.item != nil
}

// Key returns the current key.
func (m *MemtableIterator) Key() typact.Option[rel.Key] {
	return typact.Some(m.item.Key())
}

// Value returns the current value.
func (m *MemtableIterator) Value() typact.Option[[]byte] {
	return typact.Some(m.item.Value)
}

var _ dbiter.Iterator[*MemtableIterator] = (*MemtableIterator)(nil)
