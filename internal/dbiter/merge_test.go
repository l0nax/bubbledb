package dbiter

import (
	"slices"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/l0nax/bubbledb/internal/rel"
	"gitlab.com/l0nax/bubbledb/iterop"
)

var _ = Describe("Merge", func() {
	checkIterResultByKey := func(iter *MergeIterator[*MockIterator], expect ...MockData) {
		for _, exp := range expect {
			key, value := exp.Key, exp.Value

			iterKey := iter.Key()
			Expect(iterKey).To(BeEquivalentTo(key))

			iterVal := iter.Value()
			Expect(iterVal).To(BeEquivalentTo(value))

			// we ignore the return value because it will be checked later on
			_ = iter.Next()
		}

		// we iterated over all the expected keys,
		// the iterator should now be invalid
		Expect(iter.IsValid()).To(BeFalse(), "remaining entries?")
	}

	Context("iterate nicely", func() {
		i1 := NewMockIterator(
			NewMockData(rel.Key([]byte("a")), []byte("1.1")),
			NewMockData(rel.Key([]byte("b")), []byte("2.1")),
			NewMockData(rel.Key([]byte("c")), []byte("3.1")),
			NewMockData(rel.Key([]byte("e")), nil),
		)
		i2 := NewMockIterator(
			NewMockData(rel.Key([]byte("a")), []byte("1.2")),
			NewMockData(rel.Key([]byte("b")), []byte("2.2")),
			NewMockData(rel.Key([]byte("c")), []byte("3.2")),
			NewMockData(rel.Key([]byte("d")), []byte("4.2")),
		)
		i3 := NewMockIterator(
			NewMockData(rel.Key([]byte("b")), []byte("2.3")),
			NewMockData(rel.Key([]byte("c")), []byte("3.3")),
			NewMockData(rel.Key([]byte("d")), []byte("4.3")),
		)

		It("should work in order", func() {
			iter := NewMergeIterator[*MockIterator](
				i1.Clone(),
				i2.Clone(),
				i3.Clone(),
			)

			checkIterResultByKey(iter,
				NewMockData(rel.Key([]byte("a")), []byte("1.1")),
				NewMockData(rel.Key([]byte("b")), []byte("2.1")),
				NewMockData(rel.Key([]byte("c")), []byte("3.1")),
				NewMockData(rel.Key([]byte("d")), []byte("4.2")),
				NewMockData(rel.Key([]byte("e")), nil),
			)
		})

		It("should work in other order", func() {
			iter := NewMergeIterator[*MockIterator](
				i3.Clone(),
				i1.Clone(),
				i2.Clone(),
			)

			checkIterResultByKey(iter,
				NewMockData(rel.Key([]byte("a")), []byte("1.1")),
				NewMockData(rel.Key([]byte("b")), []byte("2.3")),
				NewMockData(rel.Key([]byte("c")), []byte("3.3")),
				NewMockData(rel.Key([]byte("d")), []byte("4.3")),
				NewMockData(rel.Key([]byte("e")), nil),
			)
		})
	})

	Context("Merge 2", func() {
		i1 := NewMockIterator(
			NewMockData(rel.Key([]byte("a")), []byte("1.1")),
			NewMockData(rel.Key([]byte("b")), []byte("2.1")),
			NewMockData(rel.Key([]byte("c")), []byte("3.1")),
		)
		i2 := NewMockIterator(
			NewMockData(rel.Key([]byte("d")), []byte("1.2")),
			NewMockData(rel.Key([]byte("e")), []byte("2.2")),
			NewMockData(rel.Key([]byte("f")), []byte("3.2")),
			NewMockData(rel.Key([]byte("g")), []byte("4.2")),
		)
		i3 := NewMockIterator(
			NewMockData(rel.Key([]byte("h")), []byte("1.3")),
			NewMockData(rel.Key([]byte("i")), []byte("2.3")),
			NewMockData(rel.Key([]byte("j")), []byte("3.3")),
			NewMockData(rel.Key([]byte("k")), []byte("4.3")),
		)
		i4 := NewMockIterator()

		result := []MockData{
			NewMockData(rel.Key([]byte("a")), []byte("1.1")),
			NewMockData(rel.Key([]byte("b")), []byte("2.1")),
			NewMockData(rel.Key([]byte("c")), []byte("3.1")),
			NewMockData(rel.Key([]byte("d")), []byte("1.2")),
			NewMockData(rel.Key([]byte("e")), []byte("2.2")),
			NewMockData(rel.Key([]byte("f")), []byte("3.2")),
			NewMockData(rel.Key([]byte("g")), []byte("4.2")),
			NewMockData(rel.Key([]byte("h")), []byte("1.3")),
			NewMockData(rel.Key([]byte("i")), []byte("2.3")),
			NewMockData(rel.Key([]byte("j")), []byte("3.3")),
			NewMockData(rel.Key([]byte("k")), []byte("4.3")),
		}

		It("should iterate on sorted input", func() {
			iter := NewMergeIterator[*MockIterator](
				i1.Clone(),
				i2.Clone(),
				i3.Clone(),
				i4.Clone(),
			)

			checkIterResultByKey(iter, slices.Clone(result)...)
		})

		It("should iterate on unsorted input", func() {
			iter := NewMergeIterator[*MockIterator](
				i2.Clone(),
				i4.Clone(),
				i3.Clone(),
				i1.Clone(),
			)

			checkIterResultByKey(iter, slices.Clone(result)...)
		})

		It("should iterate on reverse sorted input", func() {
			iter := NewMergeIterator[*MockIterator](
				i4.Clone(),
				i3.Clone(),
				i2.Clone(),
				i1.Clone(),
			)

			checkIterResultByKey(iter, slices.Clone(result)...)
		})
	})

	It("should merge over empty input", func() {
		iter := NewMergeIterator[*MockIterator]()
		checkIterResultByKey(iter)
	})

	It("should merge single input", func() {
		iter := NewMergeIterator[*MockIterator](
			NewMockIterator(
				NewMockData([]byte("a"), []byte("1.1")),
				NewMockData([]byte("b"), []byte("2.1")),
				NewMockData([]byte("c"), []byte("3.1")),
			),
		)

		checkIterResultByKey(iter,
			NewMockData([]byte("a"), []byte("1.1")),
			NewMockData([]byte("b"), []byte("2.1")),
			NewMockData([]byte("c"), []byte("3.1")),
		)
	})

	Describe("heapWrapper.Compare", func() {
		It("should compare lesser", func() {
			Expect(heapWrapper[*MockIterator]{
				iter: NewMockIterator(NewMockData(rel.Key([]byte("a")), []byte("1.1"))),
				idx:  0,
			}.Compare(heapWrapper[*MockIterator]{
				iter: NewMockIterator(NewMockData(rel.Key([]byte("b")), []byte("1.2"))),
				idx:  0,
			})).To(Equal(iterop.Less))

			Expect(heapWrapper[*MockIterator]{
				iter: NewMockIterator(NewMockData(rel.Key([]byte("a")), []byte("1.1"))),
				idx:  0,
			}.Compare(heapWrapper[*MockIterator]{
				iter: NewMockIterator(NewMockData(rel.Key([]byte("b")), []byte("1.1"))),
				idx:  0,
			})).To(Equal(iterop.Less))

			Expect(heapWrapper[*MockIterator]{
				iter: NewMockIterator(NewMockData(rel.Key([]byte("a")), []byte("1.1"))),
				idx:  0,
			}.Compare(heapWrapper[*MockIterator]{
				iter: NewMockIterator(NewMockData(rel.Key([]byte("a")), []byte("1.1"))),
				idx:  1,
			})).To(Equal(iterop.Less))
		})

		It("should compare greater", func() {
			Expect(heapWrapper[*MockIterator]{
				iter: NewMockIterator(NewMockData(rel.Key([]byte("b")), []byte("1.2"))),
				idx:  0,
			}.Compare(heapWrapper[*MockIterator]{
				iter: NewMockIterator(NewMockData(rel.Key([]byte("a")), []byte("1.1"))),
				idx:  0,
			})).To(Equal(iterop.Greater))

			Expect(heapWrapper[*MockIterator]{
				iter: NewMockIterator(NewMockData(rel.Key([]byte("b")), []byte("1.1"))),
				idx:  0,
			}.Compare(heapWrapper[*MockIterator]{
				iter: NewMockIterator(NewMockData(rel.Key([]byte("a")), []byte("1.1"))),
				idx:  0,
			})).To(Equal(iterop.Greater))

			Expect(heapWrapper[*MockIterator]{
				iter: NewMockIterator(NewMockData(rel.Key([]byte("a")), []byte("1.1"))),
				idx:  1,
			}.Compare(heapWrapper[*MockIterator]{
				iter: NewMockIterator(NewMockData(rel.Key([]byte("a")), []byte("1.1"))),
				idx:  0,
			})).To(Equal(iterop.Greater))
		})

		It("should compare equal", func() {
			Expect(heapWrapper[*MockIterator]{
				iter: NewMockIterator(NewMockData(rel.Key([]byte("a")), []byte("1.1"))),
				idx:  0,
			}.Compare(heapWrapper[*MockIterator]{
				iter: NewMockIterator(NewMockData(rel.Key([]byte("a")), []byte("1.1"))),
				idx:  0,
			})).To(Equal(iterop.Equal))
		})
	})
})
