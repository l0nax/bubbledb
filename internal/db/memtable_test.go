package db

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/l0nax/bubbledb/iterop"
)

var _ = Describe("Memtable", func() {
	It("should store and retrieve", func() {
		memtable := NewMemtable(0)

		memtable.Put([]byte("key1"), []byte("value1"))
		memtable.Put([]byte("key2"), []byte("value2"))
		memtable.Put([]byte("key3"), []byte("value3"))

		Expect(memtable.Get([]byte("key1")).Unwrap()).
			To(Equal([]byte("value1")))
		Expect(memtable.Get([]byte("key2")).Unwrap()).
			To(Equal([]byte("value2")))
		Expect(memtable.Get([]byte("key3")).Unwrap()).
			To(Equal([]byte("value3")))
	})

	It("should override values", func() {
		memtable := NewMemtable(0)

		memtable.Put([]byte("key1"), []byte("value1"))
		memtable.Put([]byte("key2"), []byte("value2"))
		memtable.Put([]byte("key3"), []byte("value3"))

		memtable.Put([]byte("key1"), []byte("new_value1"))
		memtable.Put([]byte("key2"), []byte("new_value2"))
		memtable.Put([]byte("key3"), []byte("new_value3"))

		Expect(memtable.Get([]byte("key1")).Unwrap()).
			To(Equal([]byte("new_value1")))
		Expect(memtable.Get([]byte("key2")).Unwrap()).
			To(Equal([]byte("new_value2")))
		Expect(memtable.Get([]byte("key3")).Unwrap()).
			To(Equal([]byte("new_value3")))
	})

	Describe("MemtableIterator", func() {
		Context("When providing normal data", func() {
			memtable := NewMemtable(0)

			memtable.Put([]byte("key1"), []byte("value1"))
			memtable.Put([]byte("key2"), []byte("value2"))
			memtable.Put([]byte("key3"), []byte("value3"))

			It("should iterate over all keys", func() {
				iter := memtable.Scan(iterop.Unbounded[KeyType](), iterop.Unbounded[KeyType]())

				Expect(iter.Key()).To(Equal([]byte("key1")))
				Expect(iter.Value()).To(Equal([]byte("value1")))
				Expect(iter.IsValid()).To(BeTrue())

				Expect(iter.Next()).To(BeTrue())

				Expect(iter.Key()).To(Equal([]byte("key2")))
				Expect(iter.Value()).To(Equal([]byte("value2")))
				Expect(iter.IsValid()).To(BeTrue())

				Expect(iter.Next()).To(BeTrue())

				Expect(iter.Key()).To(Equal([]byte("key3")))
				Expect(iter.Value()).To(Equal([]byte("value3")))
				Expect(iter.IsValid()).To(BeTrue())

				Expect(iter.Next()).To(BeFalse())
				Expect(iter.IsValid()).To(BeFalse())
			})

			It("should iterate starting through [key1, key2]", func() {
				iter := memtable.Scan(
					iterop.Included([]byte("key1")),
					iterop.Included([]byte("key2")),
				)

				Expect(iter.Key()).To(Equal([]byte("key1")))
				Expect(iter.Value()).To(Equal([]byte("value1")))
				Expect(iter.IsValid()).To(BeTrue())

				Expect(iter.Next()).To(BeTrue())

				Expect(iter.Key()).To(Equal([]byte("key2")))
				Expect(iter.Value()).To(Equal([]byte("value2")))
				Expect(iter.IsValid()).To(BeTrue())

				Expect(iter.Next()).To(BeFalse())
				Expect(iter.IsValid()).To(BeFalse())
			})

			It("should iterate starting through (key1, key3)", func() {
				iter := memtable.Scan(
					iterop.Excluded([]byte("key1")),
					iterop.Excluded([]byte("key3")),
				)

				Expect(iter.Key()).To(Equal([]byte("key2")))
				Expect(iter.Value()).To(Equal([]byte("value2")))
				Expect(iter.IsValid()).To(BeTrue())

				Expect(iter.Next()).To(BeFalse())
				Expect(iter.IsValid()).To(BeFalse())
			})
		})
	})
})
