package db

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/l0nax/bubbledb/options"
)

var _ = Describe("LSM", func() {
	It("should handle SET,GET,DELETE", func() {
		lsm := NewLSMStorageState(options.GetDefaultOptions())

		val, err := lsm.Get([]byte("unknown_key"))
		Expect(err).ToNot(HaveOccurred())
		Expect(val.IsNone()).To(BeTrue())

		err = lsm.Put([]byte("key1"), []byte("value1"))
		Expect(err).ToNot(HaveOccurred())
		err = lsm.Put([]byte("key2"), []byte("value2222"))
		Expect(err).ToNot(HaveOccurred())
		err = lsm.Put([]byte("key3"), []byte("value3333333"))
		Expect(err).ToNot(HaveOccurred())

		val, err = lsm.Get([]byte("key1"))
		Expect(err).ToNot(HaveOccurred())
		Expect(val.Unwrap()).To(Equal([]byte("value1")))

		val, err = lsm.Get([]byte("key2"))
		Expect(err).ToNot(HaveOccurred())
		Expect(val.Unwrap()).To(Equal([]byte("value2222")))

		val, err = lsm.Get([]byte("key3"))
		Expect(err).ToNot(HaveOccurred())
		Expect(val.Unwrap()).To(Equal([]byte("value3333333")))

		// check if a delete works
		{
			err = lsm.Delete([]byte("key1"))
			Expect(err).ToNot(HaveOccurred())

			val, err = lsm.Get([]byte("key1"))
			Expect(err).ToNot(HaveOccurred())
			Expect(val.IsNone()).To(BeTrue())
		}

		err = lsm.Delete([]byte("unknown_key"))
		Expect(err).ToNot(HaveOccurred(), "should not delete unknown keys")
	})

	It("should freeze memtable correctly", func() {
		lsm := NewLSMStorageState(options.GetDefaultOptions())

		err := lsm.Put([]byte("1"), []byte("value1"))
		Expect(err).ToNot(HaveOccurred())
		err = lsm.Put([]byte("2"), []byte("value2222"))
		Expect(err).ToNot(HaveOccurred())
		err = lsm.Put([]byte("3"), []byte("value3333333"))
		Expect(err).ToNot(HaveOccurred())

		err = lsm.ForceFreezeMemtable()
		Expect(err).ToNot(HaveOccurred())

		Expect(lsm.frozenMemtables.Len()).To(Equal(1), "memtable should be frozen")

		// ensure the approx size is big enough
		prevApproxSize := lsm.frozenMemtables.Slice()[0].ApproxSize()
		Expect(prevApproxSize).
			To(BeNumerically(">", 15), "approx. size of frozen memtable is too small")

		// now we override the keys but with bigger values
		err = lsm.Put([]byte("1"), []byte("xx_value1"))
		Expect(err).ToNot(HaveOccurred())
		err = lsm.Put([]byte("2"), []byte("xx_value2222"))
		Expect(err).ToNot(HaveOccurred())
		err = lsm.Put([]byte("3"), []byte("xx_value3333333"))
		Expect(err).ToNot(HaveOccurred())

		err = lsm.ForceFreezeMemtable()
		Expect(err).ToNot(HaveOccurred())

		Expect(lsm.frozenMemtables.Len()).To(Equal(2), "memtable should be frozen")

		// ensure the correct order of frozen memtables
		oldApproxSize := lsm.frozenMemtables.Slice()[1].ApproxSize()
		Expect(oldApproxSize).To(BeEquivalentTo(prevApproxSize), "wrong order of memtables?")

		Expect(lsm.frozenMemtables.Slice()[0].ApproxSize()).
			To(BeNumerically(">", prevApproxSize))
	})

	It("should automatically freeze memtables", func() {
		opts := options.GetDefaultOptions().
			WithLSMStorageSSTTargetSize(1024)
		lsm := NewLSMStorageState(opts)

		// we now insert enough elements that it should trigger a freeze
		for i := 0; i < 1_000; i++ {
			err := lsm.Put([]byte("1"), []byte("22222"))
			Expect(err).ToNot(HaveOccurred())
		}

		numMemtables := lsm.frozenMemtables.Len()
		Expect(numMemtables).To(BeNumerically(">=", 1), "no memtables frozen?")

		// a delete operation should trigger the same action
		for i := 0; i < 1_000; i++ {
			err := lsm.Delete([]byte("1"))
			Expect(err).ToNot(HaveOccurred())
		}

		Expect(lsm.frozenMemtables.Len()).
			To(BeNumerically(">", numMemtables), "no new more memtable frozen?")
	})

	It("should correctly iterate through frozen memtables", func() {
		lsm := NewLSMStorageState(options.GetDefaultOptions())

		val, err := lsm.Get([]byte("unknown_key"))
		Expect(err).ToNot(HaveOccurred())
		Expect(val.IsNone()).To(BeTrue())

		err = lsm.Put([]byte("key1"), []byte("value1"))
		Expect(err).ToNot(HaveOccurred())
		err = lsm.Put([]byte("key2"), []byte("value2222"))
		Expect(err).ToNot(HaveOccurred())
		err = lsm.Put([]byte("key3"), []byte("value3333333"))
		Expect(err).ToNot(HaveOccurred())

		// Force Freeze
		err = lsm.ForceFreezeMemtable()
		Expect(err).ToNot(HaveOccurred())

		err = lsm.Delete([]byte("key1"))
		Expect(err).ToNot(HaveOccurred())
		err = lsm.Delete([]byte("key2"))
		Expect(err).ToNot(HaveOccurred())
		err = lsm.Put([]byte("key3"), []byte("value33333"))
		Expect(err).ToNot(HaveOccurred())
		err = lsm.Put([]byte("key4"), []byte("value3333333"))
		Expect(err).ToNot(HaveOccurred())

		// Force Freeze
		err = lsm.ForceFreezeMemtable()
		Expect(err).ToNot(HaveOccurred())

		err = lsm.Put([]byte("key1"), []byte("value33333"))
		Expect(err).ToNot(HaveOccurred())
		err = lsm.Put([]byte("key3"), []byte("value33333"))
		Expect(err).ToNot(HaveOccurred())

		Expect(lsm.frozenMemtables.Len()).
			To(Equal(2), "memtable should be frozen")

		// We can now validate the data

		val, err = lsm.Get([]byte("key1"))
		Expect(err).ToNot(HaveOccurred())
		Expect(val.Unwrap()).To(Equal([]byte("value33333")))

		val, err = lsm.Get([]byte("key2"))
		Expect(err).ToNot(HaveOccurred())
		Expect(val.IsNone()).To(BeTrue())

		val, err = lsm.Get([]byte("key3"))
		Expect(err).ToNot(HaveOccurred())
		Expect(val.Unwrap()).To(Equal([]byte("value33333")))

		val, err = lsm.Get([]byte("key4"))
		Expect(err).ToNot(HaveOccurred())
		Expect(val.Unwrap()).To(Equal([]byte("value3333333")))
	})
})
