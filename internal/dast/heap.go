package dast

import "gitlab.com/l0nax/bubbledb/iterop"

// Heap implements a [binary heap] data structure.
//
// WARN: Please call [NewHeap] otherwise the heap will not be
// correctly initialized!
//
// [binary heap]: https://en.wikipedia.org/wiki/Binary_heap
type Heap[T Compare[T]] []T

// NewHeap returns a new and initialized [Heap[T]] from
// the given elements.
//
// The time complexity is O(n) where n = len(elems).
func NewHeap[T Compare[T]](elems ...T) Heap[T] {
	// Building heap: Time: O(n) | Space: O(1)
	heap := Heap[T](elems)

	// split it and process it
	lastLeafNodeIdx := (len(elems) / 2) - 1
	for i := lastLeafNodeIdx; i >= 0; i-- {
		_ = heap.down(i, len(heap))
	}

	return heap
}

// NewHeapWithCap returns an empty [Heap] with the given capacity.
func NewHeapWithCap[T Compare[T]](capacity int) Heap[T] {
	heap := make(Heap[T], 0, capacity)
	return heap
}

// Pop removes and returns the min. element from h.
// The method panics if no more elements are available in h.
//
// Time complexity: O(log n).
func (h *Heap[T]) Pop() T {
	// to pop it, we move the element to the end of the slice
	ll := len(*h) - 1
	h.swap(0, ll)
	h.down(0, ll)

	// retrieve and remove the element from the heap
	elem := (*h)[ll]
	*h = (*h)[:ll]

	return elem
}

// Push pushes the given elem onto the heap.
//
// Time complexity: O(log n).
func (h *Heap[T]) Push(elem T) {
	(*h) = append(*h, elem)
	h.up(len(*h) - 1)
}

// Remove removes the element located at idx from h
// and returns the removed element.
// The method panics if idx is out of bounds.
//
// Time complexity: O(log n).
func (h *Heap[T]) Remove(idx int) T {
	n := len(*h) - 1
	if idx != n {
		h.swap(idx, n)
		if !h.down(idx, n) {
			h.up(idx)
		}
	}

	return h.Pop()
}

// Fix fixes the heap order for h after the element has changed at idx.
func (h *Heap[T]) Fix(idx int) {
	if !h.down(idx, len(*h)) {
		h.up(idx)
	}
}

// up implements the heap-up part of the algorithm.
func (h *Heap[T]) up(idx int) {
	for {
		// Calc idx of parent
		i := (idx - 1) / 2
		if i == idx || !h.isLessAt(idx, i) {
			break
		}

		h.swap(i, idx)
		idx = i
	}
}

// down implements the heap-down part of the algorithm.
func (h *Heap[T]) down(start, end int) bool {
	i := start
	for {
		// NOTE: To handle overflow cases, j1 is an int
		j1 := 2*i + 1
		if j1 >= end || j1 < 0 {
			break
		}

		jLeft := j1
		if j2 := j1 + 1; j2 < end && h.isLessAt(j2, j1) {
			// Eq. to 2*i + 2 => as the right child
			jLeft = j2
		}

		if h.CompareAt(jLeft, i) != iterop.Less {
			break
		}

		h.swap(i, jLeft)
		i = jLeft
	}

	return i > start
}

// isLessAt compares the elements at index a and b
// and returns true if the comparsion is [iterop.Less].
//
// This method is syntactic suggar only.
func (h Heap[T]) isLessAt(a, b int) bool {
	return h.CompareAt(a, b) == iterop.Less
}

// CompareAt compares h[a] with h[b] and returns the result.
// If a or b are out of bounds of h, the method will panic.
func (h Heap[T]) CompareAt(a, b int) iterop.Ordering {
	return h[a].Compare(h[b])
}

// swap swaps elements a and b in h.
func (h Heap[T]) swap(a, b int) {
	h[a], h[b] = h[b], h[a]
}
