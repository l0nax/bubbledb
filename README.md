# BubbleDB

BubbleDB is a toy database I wrote to learn more about LSM, Databases, WAL, ...

It started by reading the challenge [LSM in a week](https://skyzh.github.io/mini-lsm/).

:warning: **NOTE:** This DB is not production ready and is intended for learning purposes.!
