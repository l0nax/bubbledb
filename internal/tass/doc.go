// Package tass is the short form of "type assisant".
// It is equivalent to libraries like
//   - https://github.com/samber/lo
//   - https://lodash.com/
//   - https://github.com/duke-git/lancet
//
// There are also similarities to Rust and methods like "map", "flat", "filter", ...
//
// The reason why those helper functions are added via this package
// is because I have special constraints/ requirements, thus developing them
// from scratch is mutch simpler.
// And I didn't want to import any 3rd party libraries.
// Another reason is that the type inference is sometimes broken with the great lo package.
// For a deep dive into this topic, see https://go.dev/blog/type-inference.
package tass
