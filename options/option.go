package options

// Options holds all options of the DB.
type Options struct {
	// LSMStorage configures the LSM storage engine.
	LSMStorage struct {
		// SSTTargetSize is the target size of a memtable (and SST) in bytes.
		//
		// Default: 256 MiB.
		SSTTargetSize int64
	}
}

// ApplyDefaults applies default values to Options if they are not set.
func (o *Options) ApplyDefaults() {
	if o.LSMStorage.SSTTargetSize == 0 {
		o.LSMStorage.SSTTargetSize = 1024 * 1024 * 256
	}
}

func (o *Options) WithLSMStorageSSTTargetSize(target int64) *Options {
	o.LSMStorage.SSTTargetSize = target
	return o
}

func GetDefaultOptions() *Options {
	opts := new(Options)
	opts.ApplyDefaults()

	return opts
}
