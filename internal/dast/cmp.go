package dast

import "gitlab.com/l0nax/bubbledb/iterop"

// Compare describes the required method for a comparator.
type Compare[T any] interface {
	// Compare compares the current element with the given one
	// and returns the [iterop.Ordering].
	Compare(T) iterop.Ordering
}
