package dbiter

import (
	"go.l0nax.org/typact"

	"gitlab.com/l0nax/bubbledb/internal/rel"
)

// Iterator defines the methods of a iterator.
type Iterator[T any] interface {
	// Key returns the key at the index of the iterator.
	// The returned key is immutable and must not be changed!
	//
	// A mutable key can be retrieved by cloning the key.
	Key() typact.Option[rel.Key]
	// Value returns the value at the index of the iterator.,
	// The returned value is immutable and must not be changed!
	//
	// A mutable value can be retrieved by cloning the value.
	Value() typact.Option[[]byte]

	Next() bool

	IsValid() bool
}
