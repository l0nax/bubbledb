// Package iterop provides helper types for iterators (iterop => iterator operations).
//
// And an implementation of std::ops::Bound from Rust, because I like the concept.
package iterop
