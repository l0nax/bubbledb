package db

import "gitlab.com/l0nax/bubbledb/internal/rel"

// KeyType is a helper type alias. Its only for syntactic suggar.
type KeyType = rel.Key

// Element represents a K/V pair with metadata.
//
// TODO: We will use this structure later!
type Element struct {
}
