package tass

// Map maps the slice s by calling fn for each element.
// It returns the resulting slice.
func Map[S ~[]E, E any, T any](s S, fn func(index int, elem E) T) []T {
	res := make([]T, len(s))

	for i, elem := range s {
		res[i] = fn(i, elem)
	}

	return res
}
