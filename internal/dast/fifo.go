package dast

import (
	"slices"

	"go.l0nax.org/typact"
)

// FIFO is a FIFO list.
type FIFO[T any] struct {
	list []T
}

// NewFIFO returns an initialized [FIFO] list.
func NewFIFO[T any](capacity int) *FIFO[T] {
	return &FIFO[T]{
		list: make([]T, 0, capacity),
	}
}

// Len returns the length of the list.
func (f *FIFO[T]) Len() int {
	return len(f.list)
}

// Push pushes the given value to the front of the list.
func (f *FIFO[T]) Push(value T) {
	f.list = slices.Insert(f.list, 0, value)
}

// First returns the first element in the list.
func (f *FIFO[T]) First() typact.Option[T] {
	if len(f.list) == 0 {
		return typact.None[T]()
	}

	return typact.Some(f.list[0])
}

// Slice returns the slice representation of f.
//
// WARN: Do NOT modify the slice!
func (f *FIFO[T]) Slice() []T {
	return f.list
}
