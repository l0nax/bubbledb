package dbiter

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.l0nax.org/typact"

	"gitlab.com/l0nax/bubbledb/internal/rel"
)

func TestDbiter(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "DBIter Suite")
}

type MockData struct {
	Key   rel.Key
	Value []byte
}

func NewMockData(key rel.Key, value []byte) MockData {
	return MockData{
		Key:   key,
		Value: value,
	}
}

type MockIterator struct {
	data []MockData
}

func NewMockIterator(data ...MockData) *MockIterator {
	return &MockIterator{
		data: data,
	}
}

func (m *MockIterator) IsValid() bool {
	return len(m.data) > 0
}

func (m *MockIterator) Next() bool {
	if len(m.data) == 0 {
		return false
	}

	m.data = m.data[1:]

	return m.IsValid()
}

func (m *MockIterator) Key() typact.Option[rel.Key] {
	if !m.IsValid() {
		panic("iterator not valid")
	}

	return typact.Some(m.data[0].Key)
}

func (m *MockIterator) Value() typact.Option[[]byte] {
	if !m.IsValid() {
		panic("iterator not valid")
	}

	return typact.Some(m.data[0].Value)
}

// Clone returns a deep clone of m.
func (m *MockIterator) Clone() *MockIterator {
	cpy := &MockIterator{
		data: make([]MockData, len(m.data)),
	}
	copy(cpy.data, m.data)

	return cpy
}
