package dbiter

import (
	"bytes"
	"cmp"
	"slices"

	"go.l0nax.org/typact"

	"gitlab.com/l0nax/bubbledb/internal/dast"
	"gitlab.com/l0nax/bubbledb/internal/rel"
	"gitlab.com/l0nax/bubbledb/iterop"
)

// heapWrapper wrapps an underlying iterator for [MergeIterator]
// and stores the additional index information.
type heapWrapper[T any] struct {
	iter Iterator[T]
	// idx is the index of the iterator when it was added
	// to the [MergeIterator].
	idx int
}

// Compare compares h with b.
func (h heapWrapper[T]) Compare(b heapWrapper[T]) iterop.Ordering {
	switch bytes.Compare(h.iter.Key().Unwrap(), b.iter.Key().Unwrap()) {
	case iterop.Greater:
		return iterop.Greater
	case iterop.Less:
		return iterop.Less
	}

	// since the keys of the iterators are equal, we
	// determine the ordering by the index within the heap.
	return cmp.Compare(h.idx, b.idx)
}

// MergeIterator is an iterator over multiple iterators.
// If the same key occurs multiple times in some iterators,
// the one with the smalest indx is prefered.
type MergeIterator[T any] struct {
	// current is the current active iterator.
	current typact.Option[heapWrapper[T]]

	// heap holds a sorted list of iterators.
	// The list is sorted from newest to oldest [Memtable].
	heap dast.Heap[heapWrapper[T]]
}

// NewMergeIterator returns an initialized [MergeIterator].
func NewMergeIterator[T any](iters ...Iterator[T]) *MergeIterator[T] {
	iter := new(MergeIterator[T])

	// we first filter out any invalid iterators.
	iters = slices.DeleteFunc(iters, func(memIter Iterator[T]) bool {
		return !memIter.IsValid()
	})

	if len(iters) == 0 {
		// we return an empty iterator, the methods will be able
		// to handle it.
		return iter
	}

	iter.heap = dast.NewHeapWithCap[heapWrapper[T]](len(iters))
	for i := range iters {
		iter.heap.Push(heapWrapper[T]{
			iter: iters[i],
			idx:  i,
		})
	}

	iter.current = typact.Some(iter.heap.Pop())

	return iter
}

// Next moves over to the next element in the iterator.
// If the next element could be retrieved, true is returned.
// Otherwise false is returned.
func (m *MergeIterator[T]) Next() bool {
	// IMPLEMENTATION NOTE:
	// Since each [MemtableIterator] returns elements in ascending order, we
	// can implement this iterator by using swap.
	//
	// We work under the assumption that the underlying iterators
	// iterate over their data (keys) in ascending order!
	current, ok := m.current.Deconstruct()
	if !ok {
		return false
	}

	// we first ensure that the current iterator and the next in heap
	// are NOT aligned (pointing at the same key).
	// This is to ensure that we can compare the keys and derive from the
	// result whether we have to swap the current iterator with the one from
	// m.heap.
	for len(m.heap) > 0 {
		// debug_assert!( "heap invariant violated")
		iter := m.heap[0]
		iterKey := iter.iter.Key().Unwrap()

		// if the keys are equal we need to move forward on one
		// iterator => we found the next non equal key.
		if !bytes.Equal(iterKey, current.iter.Key().Unwrap()) {
			break
		}

		if !iter.iter.Next() || !iter.iter.IsValid() {
			_ = m.popIterator()

			continue
		}

		// we modified the heap by calling Next() on the iterator
		// and thus we have to reorder it
		m.heap.Fix(0)
	}

	//oldCurrentKey := current.iter.Key()

	// move to the next key and validate whether the current iterator
	// is still valid.
	// If not, we move on to the next iterator.
	if !current.iter.Next() || !current.iter.IsValid() {
		m.current = m.popIterator()

		return m.IsValid()
	}

	// we now can compare the current iterator and the
	// next in the heap to decide whether we have to switch them.
	m.peekIterator().Inspect(func(next heapWrapper[T]) {
		// swap current and the first from heap if current
		// is greater than heap[0].
		if current.Compare(next) == iterop.Greater {
			// we have to swap the current iterator with the one
			// greater one from heap.
			m.current, m.heap[0] = typact.Some(next), m.current.Unwrap()

			// after swapping the elements we have to ensure
			// that the heap is still sorted correctly
			m.heap.Fix(0)
		}
	})

	return m.IsValid()
}

// IsValid returns whether the current iterator in m is valid.
func (m *MergeIterator[T]) IsValid() bool {
	if current, ok := m.current.Deconstruct(); ok {
		return current.iter.IsValid()
	}

	return false
}

func (m *MergeIterator[T]) Key() typact.Option[rel.Key] {
	if m.IsValid() {
		return m.current.Unwrap().iter.Key()
	}

	panic("iterator is not valid")
}

func (m *MergeIterator[T]) Value() typact.Option[[]byte] {
	if m.IsValid() {
		return m.current.Unwrap().iter.Value()
	}

	panic("iterator is not valid")
}

// popIterator pops the first iterator from m.heap and returns it.
// If no iterators are remaining, [typact.None] is returned.
func (m *MergeIterator[T]) popIterator() typact.Option[heapWrapper[T]] {
	if len(m.heap) == 0 {
		return typact.None[heapWrapper[T]]()
	}

	iter := m.heap.Pop()
	return typact.Some(iter)
}

// peekIterator returns the first iterator from m.heap, if available.
func (m *MergeIterator[T]) peekIterator() typact.Option[heapWrapper[T]] {
	if len(m.heap) == 0 {
		return typact.None[heapWrapper[T]]()
	}

	return typact.Some(m.heap[0])
}
