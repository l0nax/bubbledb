package tass_test

import (
	"reflect"
	"testing"

	"gitlab.com/l0nax/bubbledb/internal/tass"
)

type MyBase struct {
	Idx  int
	Data string
}

func TestMap(t *testing.T) {
	src := []MyBase{
		{0, "Foo Bar"},
		{1, "Foo Bar 1"},
		{2, "Foo Bar 2"},
		{3, "Foo Bar 3"},
		{4, "Foo Bar 3"},
	}

	mapped := tass.Map(src, func(_ int, elem MyBase) int {
		return elem.Idx
	})

	expect := []int{0, 1, 2, 3, 4}
	if !reflect.DeepEqual(mapped, expect) {
		t.Fatalf("Expected mapped object to be %+v, got %+v", expect, mapped)
	}
}

func BenchmarkMap(b *testing.B) {
	src := []MyBase{
		{0, "Foo Bar"},
		{1, "Foo Bar 1"},
		{2, "Foo Bar 2"},
		{3, "Foo Bar 3"},
		{4, "Foo Bar 3"},
	}

	b.ResetTimer()
	b.ReportAllocs()

	b.Run("Int", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			tmp := tass.Map(src, func(_ int, elem MyBase) int {
				return elem.Idx
			})
			_ = tmp
		}
	})

	b.Run("String", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			tmp := tass.Map(src, func(_ int, elem MyBase) string {
				return elem.Data
			})
			_ = tmp
		}
	})
}
