package rel

// Key is a key identifier within the database.
type Key = []byte
