package db

import (
	"sync"

	"go.l0nax.org/typact"

	"gitlab.com/l0nax/bubbledb/internal/dast"
	"gitlab.com/l0nax/bubbledb/internal/dbiter"
	"gitlab.com/l0nax/bubbledb/internal/rel"
	"gitlab.com/l0nax/bubbledb/iterop"
	"gitlab.com/l0nax/bubbledb/options"
)

// LSMStorageState represents the state of the storage engine.
type LSMStorageState struct {
	// stateLo is the lock protecting the overall state of the
	// storage engine.
	stateLo sync.RWMutex

	// memtable is the current mutable memtable.
	memtable *Memtable

	// frozenMemtables holds the frozen (immutable memtables).
	// The first element is the last frozen memtable.
	frozenMemtables *dast.FIFO[*Memtable]

	// options holds the DB engine configuration.
	options *options.Options
}

// NewLSMStorageState returns a new initialized [LSMStorageState].
func NewLSMStorageState(opts *options.Options) *LSMStorageState {
	opts.ApplyDefaults()

	// capMemtables is the default capacity.
	// Five elements is a good start for initialization.
	const capMemtables = 5

	return &LSMStorageState{
		memtable:        NewMemtable(0),
		frozenMemtables: dast.NewFIFO[*Memtable](capMemtables),
		options:         opts,
	}
}

// Scan returns an iterator for the given (lower, upper) keys.
// If lower is provided, the iterator will start from the beginning.
// If upper is provided, the iterator will iterate through all the keys starting from lower.
func (l *LSMStorageState) Scan(lower, upper iterop.Bound[[]byte]) dbiter.Iterator[*lsmIterator] {
	return newLSMIterator(l, lower, upper)
}

// Get returns the value for the given key.
func (l *LSMStorageState) Get(key []byte) (typact.Option[[]byte], error) {
	l.stateLo.RLock()
	defer l.stateLo.RUnlock()

	// fast-path check with the current memtable.
	val, ok := l.memtable.Get(key).Deconstruct()
	if ok {
		if val == nil {
			return typact.None[[]byte](), nil
		}

		return typact.Some(val), nil
	}

	// we iterate from the newest to the oldest memtable to find the key
	for _, tbl := range l.frozenMemtables.Slice() {
		val, ok := tbl.Get(key).Deconstruct()
		if !ok {
			continue
		}

		if val == nil {
			return typact.None[[]byte](), nil
		}

		return typact.Some(val), nil
	}

	// key could not be found
	return typact.None[[]byte](), nil
}

// Put stores the key-value pair in l.
func (l *LSMStorageState) Put(key, value []byte) error {
	// NOTE: We only need a read lock here because we're not
	// modifying the memtable pointer.
	l.stateLo.RLock()

	// in the future we will mark a element as deleted via
	// the metadata field.
	l.memtable.Put(key, value)
	newSize := l.memtable.ApproxSize()

	l.stateLo.RUnlock()

	return l.memtableFreezeCheck(newSize)
}

// Delete deletes the key from l.
func (l *LSMStorageState) Delete(key []byte) error {
	l.stateLo.RLock()

	// in the future we will mark a element as deleted via
	// the metadata field.
	l.memtable.Put(key, nil)
	newSize := l.memtable.ApproxSize()

	l.stateLo.RUnlock()

	return l.memtableFreezeCheck(newSize)
}

// ForceFreezeMemtable force freezes the current memtable and marks it as immutable.
func (l *LSMStorageState) ForceFreezeMemtable() error {
	l.stateLo.Lock()
	defer l.stateLo.Unlock()

	return l.unsafeForceFreezeMemtable()
}

// unsafeForceFreezeMemtable force freezes the current memtable.
//
// WARN: This method does not acquire a write lock on l!
func (l *LSMStorageState) unsafeForceFreezeMemtable() error {
	// TODO: Check if this method is getting inlined!
	l.frozenMemtables.Push(l.memtable)

	newID := l.memtable.ID() + 1
	l.memtable = NewMemtable(newID)

	return nil
}

// memtableFreezeCheck validates whether the current memtable
// needs to be frozen.
// This method is protected against concurrent calls, i.e., if two go-routines
// try to freeze the memtable concurrently, only one will succeed.
func (l *LSMStorageState) memtableFreezeCheck(approxSize int64) error {
	if approxSize < l.options.LSMStorage.SSTTargetSize {
		// no need to freeze the memtable
		return nil
	}

	l.stateLo.Lock()
	defer l.stateLo.Unlock()

	// it may be possible that another routine froze the memtable
	// by acquiring the write lock before us.
	if l.memtable.ApproxSize() < l.options.LSMStorage.SSTTargetSize {
		return nil
	}

	return l.unsafeForceFreezeMemtable()
}

// lsmIterator implements the iterator for [LSMStorageState].
type lsmIterator struct {
	iter    *dbiter.MergeIterator[*Memtable]
	isValid bool
}

func newLSMIterator(lsm *LSMStorageState, lower, upper iterop.Bound[[]byte]) *lsmIterator {
	lsm.stateLo.RLock()

	memtableIters := make([]dbiter.Iterator[*Memtable], 0, len(lsm.frozenMemtables.Slice())+1)
	memtableIters = append(memtableIters, lsm.memtable.Scan(lower, upper))

	for _, table := range lsm.frozenMemtables.Slice() {
		table := table

		memtableIters = append(memtableIters, table.Scan(lower, upper))
	}

	lsm.stateLo.RUnlock()

	return &lsmIterator{
		iter:    dbiter.NewMergeIterator[*Memtable](memtableIters...),
		isValid: true,
	}
}

// IsValid implements dbiter.Iterator.
func (l *lsmIterator) IsValid() bool {
	return l.isValid
}

// Key implements dbiter.Iterator.
func (l *lsmIterator) Key() typact.Option[rel.Key] {
	if !l.IsValid() {
		return typact.None[rel.Key]()
	}

	return l.iter.Key()
}

// Next implements dbiter.Iterator.
func (l *lsmIterator) Next() bool {
	if !l.IsValid() {
		return false
	}

	l.isValid = l.iter.Next()
	if !l.IsValid() {
		return false
	}

	// NOTE: Ensure that the value has not been deleted
	if l.iter.Value().IsNone() {
		return l.Next()
	}

	return true
}

// Value implements dbiter.Iterator.
func (l *lsmIterator) Value() typact.Option[[]byte] {
	if !l.IsValid() {
		return typact.None[[]byte]()
	}

	return l.iter.Value()
}
